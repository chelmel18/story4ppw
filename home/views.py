from django.shortcuts import render

# Create your views here.

def home(request):
    return render(request, 'home/HomeStory2.html')

def about(request):
    return render(request, 'home/AboutMeStory2.html')

def experience(request):
    return render(request, 'home/ExperiencesStory2.html')

def portfolio(request):
    return render(request, 'home/PortfolioStory2.html')

def contact(request):
    return render(request, 'home/ContactStory2.html')
