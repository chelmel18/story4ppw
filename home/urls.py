from django.urls import path

from . import views

app_name = "home" 

urlpatterns = [
    path('', views.home, name='myhome'),
    path('home/', views.home, name='myhome'),
    path('about/', views.about, name='myabout'),
    path('experiences/', views.experience, name='myexperience'),
    path('portfolio/', views.portfolio, name='myportfolio'),
    path('contact/', views.contact, name='mycontact'),
]
